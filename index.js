// This is the first file that ReactNative will run when it starts up.
//
// We jump out of here immediately and into our main entry point instead.
//
// It is possible to have React Native load our main module first, but we'd have to
// change that in both AppDelegate.m and MainApplication.java.  This would have the
// side effect of breaking other tooling like mobile-center and react-native-rename.
//
// It's easier just to leave it here.

import "./app/app.tsx"
// import Reactotron from "reactotron-react-native"

// if (__DEV__) {
//   import("./ReactotronConfig").then(() => console.log("Reactotron Configured"))
// }
// Reactotron.display({
//   name: "KNOCK KNOCK",
//   preview: "Who's there?",
//   value: "Orange.",
// })

// Reactotron.display({
//   name: "ORANGE",
//   preview: "Who?",
//   value: "Orange you glad you don't know me in real life?",
//   important: true,
// })
