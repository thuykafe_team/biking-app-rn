import { createStackNavigator } from "react-navigation"
import { LoginScreen } from "../screens/login-screen"
import { SignupScreen } from "../screens/signup-screen"
import { ResetPasswordScreen } from "../screens/reset-password-screen"

export const AuthNavigator = createStackNavigator(
  {
    login: { screen: LoginScreen },
    signup: { screen: SignupScreen },
    resetPassword: { screen: ResetPasswordScreen },
  },
  {
    headerMode: "none",
    navigationOptions: { gesturesEnabled: false },
  },
)

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 */
export const exitRoutes: string[] = ["login"]
