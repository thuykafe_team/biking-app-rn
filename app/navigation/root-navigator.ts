import { createSwitchNavigator, createStackNavigator } from "react-navigation"
import { PrimaryNavigator } from "./primary-navigator"
import { AuthNavigator } from "./auth-navigator"
import { AuthenticationCheckScreen } from "../screens/authentication-check-screen"

export const RootNavigator = createSwitchNavigator({
  authCheckStack: createStackNavigator({ authCheck: { screen: AuthenticationCheckScreen } }),
  primaryStack: { screen: PrimaryNavigator },
  authStack: { screen: AuthNavigator },
})
