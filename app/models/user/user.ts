import {
  Instance,
  SnapshotOut,
  types,
  flow,
  applySnapshot,
  getSnapshot,
  getParent,
} from "mobx-state-tree"
import {
  User as IUser,
  GetUserResult as TGetUserResult,
  Statics,
} from "../../services/authentication"
import {
  GetTokenResult,
  GetUserResult,
  UserWithCredential,
  BasicAuth,
} from "../../services/local-api"
import { withEnvironment } from "../extensions"
import * as storage from "../../utils/storage"

const USER_CREDENTIAL_STORAGE_KEY = "user-credential"
const USER_STORAGE_KEY = "user"

type StatusType = "LOGGED_IN" | "PENDING_LOGIN"
type Provider = Statics | { name: "localhost"; key: "localhost:3000" }

const DEFAULT_STATE: IUser = {
  email: "",
  id: "",
  name: "",
  firstName: "",
  lastName: "",
  permissions: [],
  age: 0,
  gender: "other",
}

/**
 * Model description here for TypeScript hints.
 */
export const UserModel = types
  .model("User")
  .props({
    state: types.optional(types.frozen(), DEFAULT_STATE),
  })
  .extend(withEnvironment)
  .volatile(self => ({
    status: "PENDING_LOGIN" as StatusType,
    provider: { name: "localhost", key: "localhost:3000" } as Provider,
  }))
  .views(self => ({
    get name(): string {
      return self.state.name
    },
    get email(): string {
      return self.state.email
    },
    get id(): string {
      return self.state.id
    },
    get firstName(): string {
      return self.state.firstName
    },
    get lastName(): string {
      return self.state.lastName
    },
    get picture(): string {
      return self.state.picture
    },
    get permissions(): string[] {
      return self.state.permissions
    },
    get age(): number {
      return self.state.age
    },
    get gender(): string {
      return self.state.gender
    },
    get isLogIn(): boolean {
      return self.status === "LOGGED_IN"
    },

    get currentUser(): IUser {
      return self.state
    },
  }))
  .preProcessSnapshot(snapshot => {
    if (!snapshot.state) {
      snapshot.state = DEFAULT_STATE
    }
    return snapshot
  })
  .actions(self => ({
    setStatus(value: StatusType) {
      self.status = value
    },
    setProvider(provider: Provider) {
      self.provider = provider
    },
  }))
  .actions(self => ({
    getCurrentUser: flow(function* getCurrentUser() {
      let snapshot: UserSnapshot
      const result: TGetUserResult = yield self.environment.authentication.getCurrentUser()

      if (result.kind === "ok") {
        snapshot = {
          state: { ...result.user },
        } as UserSnapshot
        self.setStatus("LOGGED_IN")
        self.setProvider(self.environment.authentication.currentProvider)
      } else {
        snapshot = getSnapshot(self)
        self.setStatus("PENDING_LOGIN")
      }
      applySnapshot(self, snapshot)
    }),
    retrieveSavedUser: flow(function* retrieveSavedUser() {
      let snapshot: UserSnapshot
      const saved: object = yield storage.load(USER_STORAGE_KEY)
      if (saved) {
        snapshot = { ...saved } as UserSnapshot
        self.setStatus("LOGGED_IN")
      } else {
        snapshot = getSnapshot(self)
        self.setStatus("PENDING_LOGIN")
      }
      applySnapshot(self, snapshot)
    }),
    saveCurrentUser: flow(function* saveCurrentUser() {
      const snapshot = getSnapshot(self)
      yield storage.save(USER_STORAGE_KEY, snapshot)
    }),
  }))
  .actions(self => ({
    afterCreate: flow(function* afterCreate() {
      yield self.retrieveSavedUser()
      getParent(self).authCheck()
    }),

    signInLocally: flow(function* signInLocally(auth: BasicAuth) {
      const res: GetTokenResult = yield self.environment.api.fetchToken(auth)
      if (res.kind !== "ok") {
        return
      } else if (res.kind === "ok" && res.token && res.token !== "") {
        yield storage.save(USER_CREDENTIAL_STORAGE_KEY, res.token)
      }

      const info: TGetUserResult = yield self.environment.api.getInfo(res.token)
      let snapshot: UserSnapshot

      if (info.kind !== "ok") {
        snapshot = getSnapshot(self)
        self.setStatus("PENDING_LOGIN")
      } else if (info.kind === "ok") {
        snapshot = {
          state: { ...info.user },
        } as UserSnapshot
        self.setProvider({ name: "localhost", key: "localhost:3000" })
        self.setStatus("LOGGED_IN")
      }
      applySnapshot(self, snapshot)
      yield self.saveCurrentUser()
      getParent(self).authCheck()
    }),

    signInViaThirdParty: flow(function* signInViaThirdParty(key: string) {
      self.environment.authentication.setProvider(key)
      yield self.environment.authentication.signIn()
      yield self.getCurrentUser()
      yield self.saveCurrentUser()
      getParent(self).authCheck()
    }),

    signOut: flow(function* signOut() {
      if (self.provider.name !== "localhost") yield self.environment.authentication.signOut()
      yield getParent(self).userSignOut()
    }),
  }))
  .actions(self => ({
    signUp: flow(function* signUp(userRegister: UserWithCredential) {
      const res: GetUserResult = yield self.environment.api.signUp(userRegister)
      if (res && res.kind === "ok" && "user" in res) {
        const { user: { email }, password } = userRegister
        const user: BasicAuth = { password, email }
        yield self.signInLocally(user)
      }
    }),
  }))

/**
  * Un-comment the following to omit model attributes from your snapshots (and from async storage).
  * Useful for sensitive data like passwords, or transitive state like whether a modal is open.

  * Note that you'll need to import `omit` from ramda, which is already included in the project!
  *  .postProcessSnapshot(omit(["password", "socialSecurityNumber", "creditCardNumber"]))
  */

type UserType = Instance<typeof UserModel>
export interface User extends UserType { }
type UserSnapshotType = SnapshotOut<typeof UserModel>
export interface UserSnapshot extends UserSnapshotType { }
