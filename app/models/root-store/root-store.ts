import { Instance, SnapshotOut, types, destroy, detach, flow } from "mobx-state-tree"
import { NavigationStoreModel } from "../../navigation/navigation-store"
import { UserModel } from "../user"
import { clear } from "../../utils/storage"

/**
 * A RootStore model.
 */
export const RootStoreModel = types
  .model("RootStore")
  .props({
    navigationStore: types.optional(NavigationStoreModel, {}),
    userStore: types.optional(UserModel, {}),
  })
  .actions(self => ({
    authCheck() {
      self.navigationStore.navigateToAuthCheck()
    },
  }))
  .actions(self => ({
    userSignOut: flow(function*() {
      yield clear()
      detach(self.userStore)
      destroy(self.userStore)
      self.authCheck()
    }),
  }))

/**
 * The RootStore instance.
 */
export type RootStore = Instance<typeof RootStoreModel>

/**
 * The data of a RootStore.
 */
export type RootStoreSnapshot = SnapshotOut<typeof RootStoreModel>
