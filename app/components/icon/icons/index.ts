export const icons = {
  back: require("./arrow-left@2X.png"),
  bullet: require("./bullet@2X.png"),
}

export type IconTypes = keyof typeof icons
