import { TextStyle } from "react-native"
import { color, typography } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE: TextStyle = {
  fontFamily: typography.primary,
  color: color.text,
  fontSize: 15,
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const presets = {
  /**
   * The default text styles.
   */
  default: BASE,

  /**
   * A bold version of the default text.
   */
  bold: { ...BASE, fontWeight: "bold" } as TextStyle,

  /**
   * Large headers.
   */
  header: { ...BASE, fontSize: 24, fontWeight: "bold" } as TextStyle,

  /**
   * Field labels that appear on forms above the inputs.
   */
  fieldLabel: { ...BASE, fontSize: 13, color: color.dim } as TextStyle,

  /**
   * A smaller piece of secondard information.
   */
  secondary: { ...BASE, fontSize: 9, color: color.dim } as TextStyle,

  /**
   * A sub text to add more meaning to text or compoenent above it (e.g: 'reset password' below the login button)
   */
  hint: { ...BASE, fontFamily: "Roboto-Light", color: color.palette.black } as TextStyle,

  /**
   * Similar to previous present albeit, this can be clicked, thus have diffirent color
   */
  hintLink: { ...BASE, fontFamily: "Roboto-Light", color: color.link } as TextStyle,
}

/**
 * A list of preset names.
 */
export type TextPresets = keyof typeof presets
