import * as React from "react"
import { View, TextInput } from "react-native"
import { color } from "../../theme"
import { translate } from "../../i18n"
import { Text } from "../text"
import { TextFieldProps } from "./text-field.props"
import { containerPresets, inputPresets } from "./text-field.preset"
import { mergeAll, flatten } from "ramda"

/**
 * A component which has a label and an input together.
 */
export const TextField: React.FunctionComponent<TextFieldProps> = props => {
  const {
    placeholderTx,
    placeholder,
    labelTx,
    label,
    noLabel = false,
    preset = "default",
    style: styleOverride,
    inputStyle: inputStyleOverride,
    placeholderTextColor = color.palette.lighterGrey,
    forwardedRef,
    ...rest
  } = props
  const containerStyle = mergeAll(
    flatten([containerPresets[preset] || containerPresets.default, styleOverride]),
  )
  const inputStyle = mergeAll(
    flatten([inputPresets[preset] || inputPresets.default, inputStyleOverride]),
  )
  const actualPlaceholder = placeholderTx ? translate(placeholderTx) : placeholder

  return (
    <View style={containerStyle}>
      {noLabel ? <View /> : <Text preset="fieldLabel" tx={labelTx} text={label} />}
      <TextInput
        placeholder={actualPlaceholder}
        placeholderTextColor={placeholderTextColor}
        underlineColorAndroid={color.transparent}
        {...rest}
        style={inputStyle}
        ref={forwardedRef}
      />
    </View>
  )
}
