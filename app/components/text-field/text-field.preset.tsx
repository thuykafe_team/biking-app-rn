import { ViewStyle, TextStyle } from "react-native"
import { color, spacing, typography } from "../../theme"

const CONTAINER: ViewStyle = {
  paddingVertical: spacing[3],
}

// the base styling for the TextInput
const INPUT: TextStyle = {
  fontFamily: typography.primary,
  color: color.palette.black,
  minHeight: 44,
  fontSize: 18,
  backgroundColor: color.palette.white,
}

const RADIUS = 8
const ROUND: ViewStyle = {
  borderWidth: 0.75,
  borderColor: color.line,
  borderRadius: RADIUS,
}

export const containerPresets = {
  default: {
    ...CONTAINER,
  } as ViewStyle,

  primary: {
    ...CONTAINER,
    ...ROUND,
    paddingLeft: 15,
  } as ViewStyle,
}
export const inputPresets = {
  default: {
    ...INPUT,
  } as TextStyle,
}

export type TextFieldPresetName = keyof typeof containerPresets
