import { ViewStyle, TextStyle } from "react-native"
import { color, spacing, typography } from "../../theme"

/**
 * All text will start off looking like this.
 */
const BASE_VIEW: ViewStyle = {
  paddingVertical: spacing[2],
  paddingHorizontal: spacing[2],
  borderRadius: 4,
  justifyContent: "center",
  alignItems: "center",
}

/**
 * Same as the text-field border radius
 */
const RADIUS = 8

const BASE_TEXT: TextStyle = {
  paddingHorizontal: spacing[3],
  fontFamily: typography.primary,
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const viewPresets = {
  /**
   * A smaller piece of secondard information.
   */
  primary: { ...BASE_VIEW, backgroundColor: color.palette.orange } as ViewStyle,

  /**
   * Button with proper background color and text visual
   */
  uppercase: { ...BASE_VIEW, backgroundColor: color.primary, borderRadius: RADIUS } as ViewStyle,

  /**
   * A button without extras.
   */
  link: {
    ...BASE_VIEW,
    paddingHorizontal: 0,
    paddingVertical: 0,
    alignItems: "flex-start",
  } as ViewStyle,

  /**
   * A icon button
   */
  icon: {
    ...BASE_VIEW,
    backgroundColor: color.palette.dimWhite,
    width: 50,
    height: 50,
    borderRadius: 8,
  } as ViewStyle,

  disable: {
    backgroundColor: color.disableIconBg,
  } as ViewStyle,
}

export const textPresets = {
  primary: { ...BASE_TEXT, fontSize: 9, color: color.palette.white } as TextStyle,

  uppercase: {
    ...BASE_TEXT,
    fontSize: 12,
    color: color.palette.white,
    textTransform: "uppercase",
  } as TextStyle,

  link: {
    ...BASE_TEXT,
    color: color.text,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,
}

export type ButtonPresetNames = keyof typeof viewPresets
