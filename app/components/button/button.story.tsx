import * as React from "react"
import { ViewStyle, TextStyle, Alert, View } from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { Button } from "./"

declare var module

const buttonStyleArray: ViewStyle[] = [{ paddingVertical: 100 }, { borderRadius: 0 }]

const buttonTextStyleArray: TextStyle[] = [{ fontSize: 20 }, { color: "#a511dc" }]

const buttonIconStyle: ViewStyle = { borderRadius: 10, width: 55, height: 55 }

const horizontalContainer: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  padding: 10,
}

storiesOf("Button", module)
  .addDecorator(fn => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary button.">
        <Button text="Click It" preset={["primary"]} onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="UpperCase" usage="The primary button with all uppercase text">
        <Button text="Upper!" preset={["uppercase"]} onPress={() => Alert.alert("pressed")} />
      </UseCase>
      <UseCase text="Disabled" usage="The disabled behaviour of the primary button.">
        <Button
          text="Click It"
          preset={["primary"]}
          onPress={() => Alert.alert("pressed")}
          disabled
        />
      </UseCase>
      <UseCase text="Array Style" usage="Button with array style">
        <Button
          text="Click It"
          preset={["primary"]}
          onPress={() => Alert.alert("pressed")}
          style={buttonStyleArray}
          textStyle={buttonTextStyleArray}
        />
      </UseCase>
    </Story>
  ))
  .add("With Icon", () => (
    <Story>
      <UseCase text="Icon Button" usage="The square icon button">
        <Button preset={["icon"]} onPress={() => Alert.alert("icon")} style={buttonIconStyle}>
          <Icon name="facebook" size={30} color="#900" />
        </Button>
      </UseCase>
      <UseCase text="Array Icon Button" usage="Many square icon buttons">
        <View style={horizontalContainer}>
          <Button preset={["icon"]} onPress={() => Alert.alert("icon")} style={buttonIconStyle}>
            <Icon name="facebook" size={30} color="#900" />
          </Button>
          <Button preset={["icon"]} onPress={() => Alert.alert("icon")} style={buttonIconStyle}>
            <Icon name="google" size={30} color="#900" />
          </Button>
          <Button preset={["icon"]} onPress={() => Alert.alert("icon")} style={buttonIconStyle}>
            <Icon name="apple" size={30} color="#900" />
          </Button>
        </View>
      </UseCase>
      <UseCase text="Disable icon Button" usage="The unusable square icon button">
        <Button preset={["icon", "disable"]} disabled={true} style={buttonIconStyle}>
          <Icon name="facebook" size={30} color="rgba(153,0,0,0.6)" />
        </Button>
      </UseCase>
    </Story>
  ))
