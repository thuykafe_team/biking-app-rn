import React, { useState } from "react"
import { observer } from "mobx-react"
import { ViewStyle, TextStyle, View, Image, ImageStyle } from "react-native"
import { Text } from "../../components/text"
import { Screen } from "../../components/screen"
import { Button } from "../../components/button"
import { useStores } from "../../models/root-store"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Api } from "../../services/local-api"

export interface PingScreenProps extends NavigationScreenProps<{}> {}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.angry,
}
const TEXT: TextStyle = {
  color: color.palette.white,
}
const CONTENT: TextStyle = {
  ...TEXT,
  color: "#BAB6C8",
  fontSize: 15,
  lineHeight: 22,
  marginBottom: spacing[5],
}
const PICTURE: ImageStyle = {
  height: 120,
  width: 120,
  borderRadius: 60,
  backgroundColor: color.palette.white,
  marginVertical: spacing[4],
  alignSelf: "center",
}

const DEMO: ViewStyle = {
  paddingVertical: spacing[4],
  paddingHorizontal: spacing[4],
  backgroundColor: "#5D2555",
}
const BOLD: TextStyle = { fontWeight: "bold" }
const DEMO_TEXT: TextStyle = {
  ...BOLD,
  fontSize: 13,
  letterSpacing: 2,
}

function addSpaceBetweenLetters(w: string) {
  return (w = w.replace(/([A-Z])/g, " $1").trim())
}
function capitalizeFirstLetter(word: string) {
  return word[0].toUpperCase() + word.slice(1)
}
export const PingScreen: React.FunctionComponent<PingScreenProps> = observer(props => {
  const {
    userStore: { currentUser, signOut },
  } = useStores()
  const [userInfoValue, setUserInfo] = useState({})

  React.useEffect(
    function parseStoreToState() {
      const {
        picture,
        name,
        email,
        id,
        firstName,
        lastName,
        age,
        gender,
        permissions,
      } = currentUser
      const rawUser = {
        picture,
        name,
        id,
        email,
        lastName,
        firstName,
        age,
        gender,
        permissions,
      }
      setUserInfo(rawUser)
    },
    [currentUser],
  )

  const callApi = React.useCallback(async () => {
    const demo = new Api()
    demo.setup()
    // const res = await demo.loginUser("abc@c.om", "12345678")
    const res = await demo.signUp({ email: "def@c.om", password: "123456abc" })
    console.tron.debug(res)
  }, [])

  const handleSignOut = async () => {
    await signOut()
  }
  const renderDetials = (val, key, index) => {
    const normalizedKey = addSpaceBetweenLetters(capitalizeFirstLetter(key))
    return (
      <View key={`_${index}_`}>
        <Text preset="header" style={CONTENT}>
          {`${normalizedKey}`}
          {`\n`}
          <Text>{val}</Text>
        </Text>
      </View>
    )
  }

  const renderPermissions = (pers, index) => {
    return (
      <View key={`_${index}_`}>
        <Text preset="header" style={CONTENT} text="Permissions" />
        {pers.map((key, idx) => {
          return <Text key={`permission_${idx}`}>{pers[idx]}</Text>
        })}
      </View>
    )
  }

  const renderPicture = (pic, index) => {
    return (
      <View key={`_${index}_`}>
        <Image source={{ uri: pic }} style={PICTURE} />
      </View>
    )
  }
  return (
    <Screen style={ROOT} preset="fixed">
      {Object.keys(userInfoValue).map((key, index) => {
        if (
          key === "permissions" &&
          Array.isArray(userInfoValue[key]) &&
          userInfoValue[key].length > 0
        ) {
          return renderPermissions(userInfoValue[key], index)
        }
        if (key === "picture") {
          return renderPicture(userInfoValue[key], index)
        }

        return renderDetials(userInfoValue[key], key, index)
      })}
      <Button
        style={DEMO}
        textStyle={DEMO_TEXT}
        tx="demoScreen.reactotron"
        onPress={handleSignOut}
      />
    </Screen>
  )
})
