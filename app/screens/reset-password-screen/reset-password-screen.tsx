import React, { useState } from "react"
import { observer } from "mobx-react"
import { ViewStyle, ImageStyle, TextStyle, View, Image, TouchableOpacity } from "react-native"

import { Text, Screen, Button, TextField, Wallpaper } from "../../components"
import { useStores } from "../../models/root-store"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"

import { logoBikingApp } from "../login-screen"
import { Api } from "../../services/local-api"
const whiteBG = require("../../components/wallpaper/bg.png")

export interface ResetPasswordScreenProps extends NavigationScreenProps<{}> {}

const FULL: ViewStyle = {
  flex: 1,
}
const ROOT: ViewStyle = {
  paddingHorizontal: spacing[4],
}
const LOGO: ImageStyle = {
  marginVertical: spacing[4],
  alignSelf: "center",
}
const HINT: TextStyle = {
  textAlign: "center",
  marginHorizontal: spacing[1],
}
const HINTTEXT: TextStyle = {
  ...HINT,
  paddingRight: spacing[0],
  marginRight: spacing[0],
}
const INPUTCONTAINER: ViewStyle = {
  paddingVertical: spacing[0],
  paddingHorizontal: spacing[1],
  marginVertical: spacing[2],
  marginTop: spacing[3],
}
const INPUTTEXTSTYLE: TextStyle = {
  minHeight: 24,
  fontSize: 14,
}
const LOGINBUTTON: ViewStyle = {
  marginVertical: spacing[2],
  paddingVertical: spacing[2],
  minHeight: 44,
}
const HINTLINKCONTAINER: ViewStyle = {
  paddingVertical: spacing[2],
  flexDirection: "row",
  alignContent: "flex-end",
  justifyContent: "center",
}

export const ResetPasswordScreen: React.FunctionComponent<ResetPasswordScreenProps> = observer(
  props => {
    const { userStore } = useStores()
    const [usernameValue, setUsernameValue] = useState("")

    const handleLogin = () => props.navigation.navigate("login")

    const handleResetPassword = async () => {
      const api = new Api()
      api.setup()
      await api.resetPassword(usernameValue)
      props.navigation.goBack()
    }

    return (
      <View style={FULL}>
        <Wallpaper backgroundImage={whiteBG} />
        <Screen style={ROOT} preset="scroll">
          <Image source={logoBikingApp} style={LOGO} />
          <TextField
            noLabel
            preset="primary"
            placeholderTx="loginScreen.username"
            placeholderTextColor={color.palette.offBlack}
            style={INPUTCONTAINER}
            inputStyle={[INPUTTEXTSTYLE]}
            textContentType="username"
            value={usernameValue}
            autoFocus
            autoCompleteType="off"
            onChangeText={value => setUsernameValue(value)}
          />
          <Button
            preset={["uppercase"]}
            tx="resetPasswordScreen.resetPassword"
            style={LOGINBUTTON}
            onPress={handleResetPassword}
          />
        </Screen>
        <View style={HINTLINKCONTAINER}>
          <Text preset="hint" style={HINTTEXT} tx="signupScreen.haveAccount" />
          <TouchableOpacity onPress={() => handleLogin()}>
            <Text preset="hintLink" style={HINT} tx="signupScreen.loginHere" />
          </TouchableOpacity>
        </View>
      </View>
    )
  },
)
