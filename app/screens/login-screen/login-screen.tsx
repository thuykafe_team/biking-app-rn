import React, { useState } from "react"
import { observer } from "mobx-react"
import { ViewStyle, View, Image, ImageStyle, TextStyle, TouchableOpacity } from "react-native"
import Icon from "react-native-vector-icons/FontAwesome"

import { useStores } from "../../models/root-store"
import { Wallpaper, Text, Button, Screen, TextField } from "../../components"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { DefaultThirdPartyProviders } from "../../services/authentication"
import { logoBikingApp } from "./"
const whiteBG = require("../../components/wallpaper/bg.png")

export interface LoginScreenProps extends NavigationScreenProps<{}> {}

const BOTTOMCENTER: ViewStyle = {
  justifyContent: "flex-end",
  alignItems: "center",
}

const FULL: ViewStyle = {
  flex: 1,
}
const ROOT: ViewStyle = {
  paddingHorizontal: spacing[4],
}
const LOGO: ImageStyle = {
  marginVertical: spacing[4],
  alignSelf: "center",
}
const HINT: TextStyle = {
  textAlign: "center",
  marginHorizontal: spacing[1],
}
const PADDINGVERTICAL: ViewStyle = {
  paddingVertical: spacing[2],
}

const BUTTONICON: ViewStyle = { borderRadius: 10, width: 55, height: 55 }
const BUTTONICONCONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-around",
  padding: spacing[3],
  marginHorizontal: spacing[2],
}
const INPUTCONTAINER: ViewStyle = {
  paddingVertical: spacing[0],
  paddingHorizontal: spacing[1],
  marginVertical: spacing[2],
}
const INPUTTEXTSTYLE: TextStyle = {
  minHeight: 24,
  fontSize: 14,
}
const LOGINBUTTON: ViewStyle = {
  marginVertical: spacing[2],
  paddingVertical: spacing[2],
  minHeight: 44,
}
const HINTLINKCONTAINER: ViewStyle = {
  paddingVertical: spacing[2],
  flexDirection: "row",
  alignContent: "flex-end",
  justifyContent: "center",
}

export const LoginScreen: React.FunctionComponent<LoginScreenProps> = observer(props => {
  const [usernameValue, setUsernameValue] = useState("")
  const [passwordValue, setPasswordValue] = useState("")
  const [providers, setProviders] = useState([])
  const { userStore } = useStores()

  const handleResetPassword = () => props.navigation.navigate("resetPassword")

  const handleSignUp = () => props.navigation.navigate("signup")

  const handleSignIn = async () => {
    try {
      await userStore.signInLocally({ email: usernameValue, password: passwordValue })
    } catch (error) {
      __DEV__ && console.tron.error(error)
    }
  }
  const handleSignInViaAuthenticationProvider = async (provider: string) => {
    try {
      await userStore.signInViaThirdParty(provider)
    } catch (error) {
      __DEV__ && console.tron.error(error)
    }
  }

  React.useEffect(() => {
    const statics = DefaultThirdPartyProviders
    setProviders(statics)
  }, [])

  return (
    <View style={FULL}>
      <Wallpaper backgroundImage={whiteBG} />
      <Screen style={ROOT} preset="scroll">
        <Image source={logoBikingApp} style={LOGO} />
        <Text preset="hint" style={HINT} tx="loginScreen.continueWith" />
        <View style={BUTTONICONCONTAINER}>
          {providers.map((prov, idx) => {
            if (prov.name === "twitter") {
              return (
                <Button
                  preset={["icon", "disable"]}
                  disabled={true}
                  onPress={() => handleSignInViaAuthenticationProvider(`${prov.key}`)}
                  style={BUTTONICON}
                  key={`_${prov.key}_${idx}`}
                >
                  <Icon name={prov.name} size={30} color={color.disableIcon} />
                </Button>
              )
            }
            return (
              <Button
                preset={["icon"]}
                onPress={() => handleSignInViaAuthenticationProvider(`${prov.key}`)}
                style={BUTTONICON}
                key={`_${prov.key}_${idx}`}
              >
                <Icon name={prov.name} size={30} color={color.icon} />
              </Button>
            )
          })}
        </View>
        <View style={{ ...PADDINGVERTICAL }}>
          <Text preset="hint" style={HINT} tx="loginScreen.signinEmail" />
        </View>
        <TextField
          noLabel
          preset="primary"
          placeholderTx="loginScreen.username"
          placeholderTextColor={color.palette.offBlack}
          style={[INPUTCONTAINER, { marginTop: spacing[3] }]}
          inputStyle={[INPUTTEXTSTYLE]}
          textContentType="username"
          value={usernameValue}
          onChangeText={value => setUsernameValue(value)}
        />
        <TextField
          noLabel
          preset="primary"
          placeholderTx="loginScreen.password"
          placeholderTextColor={color.palette.offBlack}
          style={[INPUTCONTAINER]}
          inputStyle={[INPUTTEXTSTYLE]}
          textContentType="password"
          secureTextEntry
          autoCompleteType="password"
          value={passwordValue}
          onChangeText={value => setPasswordValue(value)}
        />
        <Button
          preset={["uppercase"]}
          tx="loginScreen.login"
          style={LOGINBUTTON}
          onPress={handleSignIn}
        />

        <View style={[PADDINGVERTICAL, BOTTOMCENTER]}>
          <TouchableOpacity onPress={handleResetPassword}>
            <Text preset="hintLink" style={HINT} tx="loginScreen.resetPassword" />
          </TouchableOpacity>
        </View>
      </Screen>
      <View style={HINTLINKCONTAINER}>
        <Text preset="hint" style={HINT} tx="loginScreen.dontHaveAccount" />
        <TouchableOpacity onPress={() => handleSignUp()}>
          <Text preset="hintLink" style={HINT} tx="loginScreen.signupHere" />
        </TouchableOpacity>
      </View>
    </View>
  )
})
