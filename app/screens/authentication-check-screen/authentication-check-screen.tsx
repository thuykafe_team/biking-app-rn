import * as React from "react"
import { observer } from "mobx-react"
import { ViewStyle } from "react-native"
import { Text, Screen } from "../../components"
import { useStores } from "../../models/root-store"
import { color } from "../../theme"
import { NavigationScreenProps } from "react-navigation"

export interface AuthenticationCheckScreenProps extends NavigationScreenProps<{}> {}

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
}

export const AuthenticationCheckScreen: React.FunctionComponent<
  AuthenticationCheckScreenProps
> = observer(props => {
  const {
    userStore: { isLogIn },
  } = useStores()

  React.useEffect(() => {
    if (isLogIn) {
      // TODO pass user parameter in primaryStack
      props.navigation.navigate("primaryStack")
    } else {
      props.navigation.navigate("authStack")
    }
    return () => {}
  }, [])

  return (
    <Screen style={ROOT} preset="scroll">
      <Text preset="header" text="Check is authorized" />
    </Screen>
  )
})
