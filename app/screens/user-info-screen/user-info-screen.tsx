import React, { useState } from "react"
import { observer } from "mobx-react"
import { ViewStyle, TextStyle, View, Image, ImageStyle } from "react-native"
import { Screen, Button, TextField, Wallpaper } from "../../components"
import { useStores } from "../../models/root-store"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { Api } from "../../services/local-api"
const whiteBG = require("../../components/wallpaper/bg.png")

export interface UserInfoScreenProps extends NavigationScreenProps<{}> {}

const ROOT: ViewStyle = {
  paddingHorizontal: spacing[4],
}
const PICTURE: ImageStyle = {
  height: 120,
  width: 120,
  borderRadius: 60,
  backgroundColor: color.palette.white,
  marginVertical: spacing[4],
  alignSelf: "center",
  borderColor: color.primary,
  borderWidth: 2.25,
}
const FULL: ViewStyle = {
  flex: 1,
}
const INPUTCONTAINER: ViewStyle = {
  paddingVertical: spacing[0],
  paddingHorizontal: spacing[1],
  marginVertical: spacing[2],
}
const INPUTTEXTSTYLE: TextStyle = {
  minHeight: 24,
  fontSize: 14,
}
const LOGINBUTTON: ViewStyle = {
  marginVertical: spacing[2],
  paddingVertical: spacing[2],
  minHeight: 44,
}

function addSpaceBetweenLetters(w: string) {
  return (w = w.replace(/([A-Z])/g, " $1").trim())
}
function capitalizeFirstLetter(word: string) {
  return word[0].toUpperCase() + word.slice(1)
}
export const UserInfoScreen: React.FunctionComponent<UserInfoScreenProps> = observer(props => {
  const {
    userStore: { currentUser, signOut },
  } = useStores()
  const [userInfoValue, setUserInfo] = useState({})

  React.useEffect(
    function parseStoreToState() {
      const sortProperties = raw => {
        const { picture, name, email, id, firstName, lastName, age, gender, permissions } = raw
        return {
          picture,
          id,
          name,
          email,
          firstName,
          lastName,
          age,
          gender,
          permissions,
        }
      }
      const rawUser = sortProperties(currentUser)
      setUserInfo(rawUser)
    },
    [currentUser],
  )

  const callApi = React.useCallback(async () => {
    const demo = new Api()
    demo.setup()
    // const res = await demo.loginUser("abc@c.om", "12345678")
    const res = await demo.signUp({ email: "def@c.om", password: "123456abc" })
    console.tron.debug(res)
  }, [])

  const handleSignOut = async () => {
    await signOut()
  }
  const renderDetials = (val = "Empty", key, index) => {
    const normalizedKey = addSpaceBetweenLetters(capitalizeFirstLetter(key))
    return (
      <View key={`_${index}_`}>
        <TextField
          editable={false}
          preset="primary"
          label={normalizedKey}
          style={[INPUTCONTAINER]}
          inputStyle={[INPUTTEXTSTYLE]}
          value={val}
        />
      </View>
    )
  }

  const renderPermissions = (pers, index) => {
    const permissionsToString =
      pers.length > 0 ? pers.reduce((acc, curr) => `${acc}\n\n${curr}`) : "Empty"
    return (
      <View key={`_${index}_`}>
        <TextField
          editable={false}
          preset="primary"
          labelTx="userInfoScreen.permissions"
          style={[INPUTCONTAINER]}
          inputStyle={[INPUTTEXTSTYLE]}
          value={permissionsToString}
          multiline
          numberOfLines={pers.length + 1}
        />
      </View>
    )
  }

  const renderPicture = (pic, index) => {
    return (
      <View key={`_${index}_`}>
        <Image source={{ uri: pic }} style={PICTURE} />
      </View>
    )
  }
  return (
    <View style={FULL}>
      <Wallpaper backgroundImage={whiteBG} />
      <Screen style={ROOT} preset="scroll">
        {Object.keys(userInfoValue).map((key, index) => {
          if (
            key === "permissions" &&
            Array.isArray(userInfoValue[key]) &&
            userInfoValue[key].length > 0
          ) {
            return renderPermissions(userInfoValue[key], index)
          }
          if (key === "picture") {
            return renderPicture(userInfoValue[key], index)
          }

          return renderDetials(userInfoValue[key], key, index)
        })}
        <Button
          preset={["uppercase"]}
          tx="userInfoScreen.signOut"
          style={LOGINBUTTON}
          onPress={handleSignOut}
        />
      </Screen>
    </View>
  )
})
