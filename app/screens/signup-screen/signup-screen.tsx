import * as React from "react"
import { observer } from "mobx-react"
import { ViewStyle, View, Image, ImageStyle, TextStyle, TouchableOpacity } from "react-native"
import { Screen, Text, Wallpaper, Button, TextField } from "../../components"
import { TextFieldProps } from "../../components/text-field/text-field.props"
import Icon from "react-native-vector-icons/FontAwesome"
import validate from "validate.js"

import { useStores } from "../../models/root-store"
import { color, spacing } from "../../theme"
import { NavigationScreenProps } from "react-navigation"
import { logoBikingApp } from "../login-screen"
import { DefaultThirdPartyProviders } from "../../services/authentication"
import { UserWithCredential } from "../../services/local-api"
const whiteBG = require("../../components/wallpaper/bg.png")

export interface SignupScreenProps extends NavigationScreenProps<{}> { }

const FULL: ViewStyle = {
  flex: 1,
}
const ROOT: ViewStyle = {
  paddingHorizontal: spacing[4],
}
const HEADER: ViewStyle = {
  flexDirection: "row",
  paddingVertical: spacing[3],
}
const LEFTCONTAINER: ViewStyle = {
  justifyContent: "center",
  alignContent: "center",
}
const HINT: TextStyle = {
  textAlign: "left",
  textTransform: "capitalize",
  padding: spacing[2],
  paddingLeft: spacing[0],
  marginHorizontal: spacing[1],
}
const BUTTONCONTAINER: ViewStyle = {
  justifyContent: "space-around",
  alignContent: "center",
  padding: spacing[1],
  marginLeft: spacing[4],
}
const BUTTON: ViewStyle = {
  height: 50,
  width: 50,
  margin: spacing[2],
}
const LOGO: ImageStyle = {
  marginVertical: spacing[4],
  alignSelf: "center",
  transform: [{ scaleY: 1.05 }, { scaleX: 1.15 }, { translateX: 25 }],
}
const INPUTCONTAINER: ViewStyle = {
  paddingVertical: spacing[0],
  paddingHorizontal: spacing[1],
  marginVertical: spacing[2],
  marginTop: spacing[3],
}
const FALSYINPUTCONTAINER: ViewStyle = {
  ...INPUTCONTAINER,
  borderColor: color.palette.angry,
}
const INPUTSTYLE: TextStyle = {
  minHeight: 24,
  fontSize: 14,
}
const FALSYINPUTSTYLE: TextStyle = {
  ...INPUTSTYLE,
  color: color.palette.angry,
}
const SIGNUPBUTTON: ViewStyle = {
  marginVertical: spacing[2],
  paddingVertical: spacing[2],
  minHeight: 44,
}
const HINTLINKCONTAINER: ViewStyle = {
  paddingVertical: spacing[2],
  flexDirection: "row",
  alignContent: "flex-end",
  justifyContent: "center",
}
/**
 * Check the input text is correctly formatted
 * @param value input from component text-field
 * @returns boolean variable
 */
const useVailidateValue = (value: string): boolean => {
  const [validatedValue, setValidation] = React.useState(false)
  React.useEffect(() => {
    function doSomeValidation(value) {
      let check = false
      if (value.length > 6) check = true
      setValidation(check)
    }
    doSomeValidation(value)
  })
  return validatedValue
}

interface ValidateTextFieldProps extends TextFieldProps {
  constraints: { [k: string]: any }
  validateAttributes: { [k: string]: any }
}

const ValidateTextField: React.FunctionComponent<ValidateTextFieldProps> = props => {
  const { constraints, validateAttributes, ...rest } = props
  const [hasConstraintsError, setConstraintsError] = React.useState(undefined)

  const [initRender, setInitRender] = React.useState(true)
  React.useEffect(() => setInitRender(false), [])

  React.useEffect(() => {
    const error = initRender
      ? undefined
      : validate(validateAttributes, constraints, { format: "flat" })
    setConstraintsError(error)
  }, [validateAttributes, constraints])

  return (
    <TextField
      noLabel
      preset="primary"
      placeholderTextColor={color.palette.offBlack}
      style={hasConstraintsError ? FALSYINPUTCONTAINER : INPUTCONTAINER}
      inputStyle={hasConstraintsError ? FALSYINPUTSTYLE : INPUTSTYLE}
      {...rest}
    />
  )
}

export const SignupScreen: React.FunctionComponent<SignupScreenProps> = observer(props => {
  const { userStore } = useStores()
  const [firstNameValue, setFirstNameValue] = React.useState("")
  const [lastNameValue, setlastNameValue] = React.useState("")
  const [emailValue, setEmailValue] = React.useState("")
  const [passwordValue, setPasswordValue] = React.useState("")
  const [confirmPasswordValue, setConfirmPasswordValue] = React.useState("")
  const [providers, setProviders] = React.useState([])

  /**
   * set all providers at ComponentDidMount
   */
  React.useEffect(() => {
    const statics = DefaultThirdPartyProviders
    setProviders(statics)

    return () => { }
  }, [])

  /**
   * anounce the chosen provider to Mobx-state-tree
   */
  const handleSignUpViaAuthenticationProvider = React.useMemo(
    () => async (provider: string) => {
      try {
        await userStore.signInViaThirdParty(provider)
      } catch (error) {
        __DEV__ && console.tron.error(error)
      }
    },
    [userStore],
  )

  const handleLogin = React.useMemo(() => () => props.navigation.navigate("login"), [
    props.navigation,
  ])

  const handleSignUp = React.useMemo(
    () => async () => {
      const registerUser: UserWithCredential = {
        user: {
          email: emailValue,
          lastName: lastNameValue,
          firstName: firstNameValue,
        },
        password: passwordValue,
      }
      try {
        await userStore.signUp(registerUser)
      } catch (error) {
        __DEV__ && console.tron.error(error)
      }
    },
    [emailValue, firstNameValue, lastNameValue, passwordValue, userStore],
  )

  return (
    <View style={FULL}>
      <Wallpaper backgroundImage={whiteBG} />
      <Screen style={ROOT} preset="scroll">
        <View style={HEADER}>
          <View style={LEFTCONTAINER}>
            <Text preset="hint" style={HINT} tx="loginScreen.continueWith" />
            <View style={BUTTONCONTAINER}>
              {providers.map((prov, idx) => {
                if (prov.name === "twitter") {
                  return (
                    <Button
                      preset={["icon", "disable"]}
                      disabled={true}
                      onPress={() => handleSignUpViaAuthenticationProvider(`${prov.key}`)}
                      style={BUTTON}
                      key={`_${prov.key}_${idx}`}
                    >
                      <Icon name={prov.name} size={30} color={color.disableIcon} />
                    </Button>
                  )
                }
                return (
                  <Button
                    preset={["icon"]}
                    onPress={() => handleSignUpViaAuthenticationProvider(`${prov.key}`)}
                    style={BUTTON}
                    key={`_${prov.key}_${idx}`}
                  >
                    <Icon name={prov.name} size={30} color={color.icon} />
                  </Button>
                )
              })}
            </View>
          </View>

          <Image source={logoBikingApp} style={LOGO} resizeMode="stretch" />
        </View>

        <Text preset="hint" style={HINT} tx="signupScreen.orSignupBelow" />

        {React.useMemo(() => {
          return (
            <ValidateTextField
              placeholderTx="signupScreen.email"
              textContentType="emailAddress"
              value={emailValue}
              onChangeText={value => setEmailValue(value)}
              validateAttributes={{ email: emailValue }}
              constraints={{
                email: {
                  presence: true,
                  email: true,
                },
              }}
            />
          )
        }, [emailValue])}
        {React.useMemo(() => {
          return (
            <ValidateTextField
              placeholderTx="signupScreen.firstname"
              textContentType="name"
              value={firstNameValue}
              onChangeText={value => setFirstNameValue(value)}
              validateAttributes={{ firstName: firstNameValue }}
              constraints={{
                firstName: {
                  type: "string",
                },
              }}
            />
          )
        }, [firstNameValue])}
        {React.useMemo(() => {
          return (
            <ValidateTextField
              placeholderTx="signupScreen.lastname"
              textContentType="name"
              value={lastNameValue}
              onChangeText={value => setlastNameValue(value)}
              validateAttributes={{ lastName: lastNameValue }}
              constraints={{
                lastName: {
                  type: "string",
                },
              }}
            />
          )
        }, [lastNameValue])}
        {React.useMemo(() => {
          return (
            <ValidateTextField
              placeholderTx="loginScreen.password"
              textContentType="password"
              secureTextEntry
              autoCompleteType="password"
              value={passwordValue}
              onChangeText={value => setPasswordValue(value)}
              validateAttributes={{ password: passwordValue }}
              constraints={{
                password: {
                  presence: true,
                  length: { minimum: 8 },
                },
              }}
            />
          )
        }, [passwordValue])}
        {React.useMemo(() => {
          return (
            <ValidateTextField
              placeholderTx="signupScreen.confirmPassword"
              textContentType="password"
              secureTextEntry
              autoCompleteType="password"
              value={confirmPasswordValue}
              onChangeText={value => setConfirmPasswordValue(value)}
              validateAttributes={{
                password: passwordValue,
                confirmPassword: confirmPasswordValue,
              }}
              constraints={{
                confirmPassword: {
                  equality: "password",
                },
              }}
            />
          )
        }, [confirmPasswordValue, passwordValue])}
        <Button
          preset={["uppercase"]}
          tx="signupScreen.signup"
          style={SIGNUPBUTTON}
          onPress={handleSignUp}
        />
      </Screen>
      <View style={HINTLINKCONTAINER}>
        <Text
          preset="hint"
          style={{ ...HINT, paddingRight: spacing[0], marginRight: spacing[0] }}
          tx="signupScreen.haveAccount"
        />
        <TouchableOpacity onPress={() => handleLogin()}>
          <Text preset="hintLink" style={HINT} tx="signupScreen.loginHere" />
        </TouchableOpacity>
      </View>
    </View>
  )
})
