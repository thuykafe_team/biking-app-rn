import * as Types from "./authentication.types"
import { FacebookProvider, GoogleProvider } from "./statics"
import { find, propEq } from "ramda"

export class Authentication implements Types.IAuthentication {
  private _currentProvider: Types.Provider

  private _currentUser: Types.User

  private _providers: Types.Provider[]

  get currentUser(): Types.User {
    return this._currentUser
  }

  get currentProvider(): Types.ThirdPartyProvider {
    return this._currentProvider.toString()
  }

  async setup() {
    // TODO
    /**
     * 1/ Promise ( resolve => {
     * try {
     *  getCurrentUser() from 'async-storage'
     *  if (isGoodUser(user)) resolve('everything goods, celebrate time' 🙌)
     *  else {
     *    use is empty, setupProviders
     *    handle logic
     *  }
     * } catch {
     *  something happens, request authenticate again or refresh app??
     *  reject('Error occurs ${e}')
     * }
     */
    await this.getCurrentUser()
      .then(res => {
        if ("user" in res) {
          this._currentUser = res.user
        }
      })
      .catch(e => {
        if (__DEV__) console.error(e)
      })
    this.setupProviders()
  }

  setupProviders = (): void => {
    const facebookProvider = new FacebookProvider()
    const googleProvider = new GoogleProvider()
    // Too lazy to create a account on Twitter
    // cosnt twitterProvider = new TwitterProvider()

    Promise.all([facebookProvider.setup(), googleProvider.setup()])
      .then()
      .catch(e => Error("Can not initialize providers, error: " + e))
    this._providers = [facebookProvider, googleProvider]
  }

  async getCurrentUser(): Promise<Types.GetUserResult> {
    if (!this._currentProvider) {
      return { kind: "something went wrong" }
    }
    const response = await this._currentProvider.getUser()
    if (!response.ok) {
      const problem = Types.getGeneralUserProblem(response)
      if (problem) return problem
    }
    const neatlyConvertedUser = (raw: object): Types.User => {
      const FBResponseConvert = (resp): Types.User => {
        const {
          id,
          name,
          email,
          first_name: firstName,
          last_name: lastName,
          permissions,
          picture,
        } = resp
        const convertedPermissions = permissions.data.map(each => each.permission)
        return {
          id,
          name,
          firstName,
          lastName,
          email,
          permissions: convertedPermissions,
          picture: picture.data.url,
        }
      }

      const GGResponseConvert = (resp): Types.User => {
        const { user, scopes: permissions } = resp
        const { id, email, name, givenName: firstName, familyName: lastName, photo: picture } = user
        return {
          id,
          name,
          firstName,
          lastName,
          email,
          permissions,
          picture,
        }
      }

      switch (this._currentProvider.PROVIDER_ID) {
        case "facebook.com":
          return FBResponseConvert(raw)
        case "google.com":
          return GGResponseConvert(raw)
        default:
          return {
            id: "",
            email: "",
            firstName: "",
            lastName: "",
            name: "",
            permissions: [],
            picture: "",
          }
      }
    }

    // transform data into desired format and readable data
    try {
      const rawData = response.data
      const result: Types.User = neatlyConvertedUser(rawData)
      return { kind: "ok", user: result }
    } catch {
      return { kind: "user/bad-data" }
    }
  }

  setProvider(key: string): Types.GetProviderResult {
    const provider = find(propEq("PROVIDER_ID", key))(this._providers)
    if (provider) {
      this._currentProvider = provider
      return { kind: "ok", provider: provider }
    } else {
      return { kind: "provider/no-such-provider" }
    }
  }

  async signIn() {
    try {
      await this._currentProvider.signIn()
    } catch (e) {}
  }

  async signOut() {
    try {
      await this._currentProvider.signOut()
    } catch {}
  }
}
