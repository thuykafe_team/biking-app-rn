// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export interface IAuthentication {
  setup: () => Promise<void>
  setProvider: (id: string) => GetProviderResult
  getCurrentUser: (foo?: any) => Promise<GetUserResult>
}

type UserGender = "male" | "female" | "other"

export interface User {
  id: string
  name: string
  email: string
  firstName?: string
  lastName?: string
  picture?: string
  permissions: string[]
  age?: number | 0
  gender?: UserGender | "other"
}

export type GetUserResult = { kind: "ok"; user: User } | GeneralUserProblem

export interface ThirdPartyProvider {
  name: string
  key: string
}
export const Facebook: ThirdPartyProvider = {
  name: "facebook",
  key: "facebook.com",
}
export const Google: ThirdPartyProvider = {
  name: "google",
  key: "google.com",
}
export const Twitter: ThirdPartyProvider = {
  name: "twitter",
  key: "twitter.com",
}
export type Statics = typeof Facebook | typeof Google | typeof Twitter

export const DefaultThirdPartyProviders: ThirdPartyProvider[] = [Facebook, Google, Twitter]

export interface Provider {
  PROVIDER_ID: string
  toString: () => ThirdPartyProvider
  setup: () => Promise<void>
  signIn: () => Promise<void>
  signOut: () => Promise<void>
  getUser: () => Promise<UserResponse<object | null>>
  credential: (token: string, secret: string) => Credential
}
export type GetProviderResult = { kind: "ok"; provider: Provider } | GeneralProviderProblem

export interface Credential {
  providerId: string
  token: string
  secret: string
}

interface SomethingWentWrongProblem {
  kind: "something went wrong"
}
export type GeneralUserProblem =
  | { kind: "user/not-found" }
  | { kind: "user/bad-data" }
  | { kind: "user/not-have-credential" }
  | { kind: "user/google/sign-in-required" }
  | { kind: "user/google/sign-in-cancelled" }
  | { kind: "user/google/in-progress" }
  | { kind: "user/google/play-service-not-available" }
  | SomethingWentWrongProblem

export type GeneralProviderProblem =
  | { kind: "provider/no-such-provider" }
  | SomethingWentWrongProblem

/**
 * Not have any usage at anywhere at the moment
 */
export function getGeneralProviderProblem(response: any): GeneralProviderProblem | void {
  switch (response.problem) {
    case "NO_SUCH_PROVIDER":
      return { kind: "provider/no-such-provider" }
    default:
      return { kind: "provider/no-such-provider" }
  }
}

export function getGeneralUserProblem(response: UserResponse<any>): GeneralUserProblem | void {
  switch (response.problem) {
    default:
      return { kind: "user/not-found" }
  }
}

interface UserErrorResponse<T> {
  ok: false
  problem: any

  data?: T
}
interface UserOkResponse<T> {
  ok: true
  problem: null

  data?: T
}

export type UserResponse<T, U = T> = UserErrorResponse<U> | UserOkResponse<T>
