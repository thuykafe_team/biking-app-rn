import { NativeModules } from "react-native"
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk"
import { GoogleSignin, statusCodes } from "@react-native-community/google-signin"
import {
  GOOGLE_WEB_CLIENT_ID,
  TWITTER_CONSUMER_KEY,
  TWITTER_CONSUMER_SECRET,
} from "react-native-dotenv"

import * as Types from "./authentication.types"
import * as storage from "../../utils/storage"
const { RNTwitterSignIn } = NativeModules

const USER_CREDENTIAL_STORAGE_KEY = "user-credential"

const saveCredential = async (credential: Types.Credential): Promise<void> => {
  await storage.remove(USER_CREDENTIAL_STORAGE_KEY)
  await storage.save(USER_CREDENTIAL_STORAGE_KEY, credential)
}
const removeCredential = async (): Promise<void> => {
  await storage.remove(USER_CREDENTIAL_STORAGE_KEY)
}

const Credential = (providerId: string, token: string, secret: string): Types.Credential => {
  return { providerId, token, secret }
}

export class FacebookProvider implements Types.Provider {
  toString = () => Types.Facebook
  PROVIDER_ID: string = this.toString().key

  /**
   * Do nothing for this provider, because everything have been configured manually
   */
  setup = async () => {}

  requestSignInWithPermissions = (
    permissions: string[] = ["public_profile", "email"],
  ): Promise<any> =>
    LoginManager.logInWithPermissions(permissions).then(result => {
      // TODO implement problem
      if (result.isCancelled) {
        console.tron.display({
          name: "LOGIN",
          preview: "Login not success",
          value: "Login was cancelled",
        })
      } else {
        console.tron.display({
          name: "LOGIN",
          preview: "Login success",
          value: "Login was successful with permissions: " + result.grantedPermissions.toString(),
        })
      }
    })

  credential = (token: string, secret: string) => Credential(this.PROVIDER_ID, token, secret)

  signIn = async () => {
    const data = await AccessToken.getCurrentAccessToken()
    if (!data) {
      try {
        const havePermissions = await this.requestSignInWithPermissions()
        if (havePermissions) {
          this.signIn()
        }
      } catch (error) {}
    } else {
      saveCredential(this.credential(data.accessToken, ""))
    }
  }

  getUser = async (): Promise<Types.UserResponse<object | null>> =>
    new Promise<Types.UserResponse<object>>(resolve => {
      const _responseInfoCallback = (error?: object, result?: object) => {
        if (error) {
          resolve({ ok: false, problem: error, data: error })
        } else {
          resolve({ ok: true, problem: null, data: result })
        }
      }
      const infoRequest = new GraphRequest(
        "/me",
        {
          httpMethod: "GET",
          version: "v5.0",
          parameters: {
            fields: {
              string: "id,name,email,first_name,last_name,picture{url},permissions{permission}",
            },
          },
        },
        _responseInfoCallback,
      )
      // Start the graph request.
      new GraphRequestManager().addRequest(infoRequest).start()
    })

  signOut = async () => {
    LoginManager.logOut()
    await removeCredential()
  }
}

export class GoogleProvider implements Types.Provider {
  toString = () => Types.Google
  PROVIDER_ID: string = this.toString().key

  setup = async () => {
    GoogleSignin.configure({
      scopes: ["email", "profile", "openid"],
      offlineAccess: false,
      webClientId: GOOGLE_WEB_CLIENT_ID || "",
    })
  }

  credential = (token: string, secret: string) => Credential(this.PROVIDER_ID, token, secret)

  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices()
      const userInfo = await GoogleSignin.signIn()
      await saveCredential(this.credential(userInfo.idToken, ""))

      /**
       * return userInfo={
       *    idToken: string,
       *    serverAuthCode: string,
       *    scopes: Array<string>, // on iOS this is empty array if no additional scopes are defined
       *    user: {
       *      email: string,
       *      id: string,
       *      givenName: string,
       *      familyName: string,
       *      photo: string, // url
       *      name: string // full name
       * }}
       */
    } catch (error) {
      // TODO remember to implement types.user-problem in this section
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  }

  getUser = async (): Promise<Types.UserResponse<object | null>> => {
    try {
      const currentUser = await GoogleSignin.getCurrentUser()
      if (currentUser) {
        return { ok: true, problem: null, data: currentUser }
      }
    } catch (e) {
      return { ok: false, problem: e }
    }
    return { ok: false, problem: {} }
  }

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess()
      await GoogleSignin.signOut()
    } catch (error) {}
    await removeCredential()
  }
}

export class TwitterProvider implements Types.Provider {
  toString = () => Types.Twitter
  PROVIDER_ID: string = this.toString().key

  setup = async () => {
    await RNTwitterSignIn.init(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
  }

  credential = (token: string, secret: string) => Credential(this.PROVIDER_ID, token, secret)

  signIn = async () => {
    const response = await RNTwitterSignIn.logIn()

    await saveCredential(this.credential(response.authToken, response.authTokenSecret))
  }

  getUser = async (): Promise<Types.UserResponse<object | null>> => {
    return { ok: false, problem: {} }
  }

  signOut = async () => {
    RNTwitterSignIn.logOut()
    await removeCredential()
  }
}
