// @ts-ignore
import { Authentication } from "./authentication"
import * as Types from "./authentication.types"

// now mockAuthentication is a a mock constructor
jest.mock("./authentication")

// this syntax is applied for ts only
// eslint-disable-next-line @typescript-eslint/no-angle-bracket-type-assertion
const mockAuthentication = <jest.Mock<Authentication>>Authentication

const mockSetup = jest.fn().mockName("setup")
const mockGetCurrentUser = jest.fn().mockName("getCurrentName")
const mockSetProvider = jest.fn().mockName("setProvider")
const mockSetupProviders = jest.fn().mockName("setupProviders")
// @ts-ignore
mockAuthentication.mockImplementation(() => {
  return {
    setup: mockSetup,
    getCurrentUser: mockGetCurrentUser,
    setProvider: mockSetProvider,
    setupProviders: mockSetupProviders,
  }
})

describe("Authentication", () => {
  beforeEach(() => {
    mockSetup.mockClear()
    mockAuthentication.mockClear()
  })

  test("check before each test suit instance is correct created", () => {
    const instance = new mockAuthentication()
    expect(mockAuthentication).toHaveBeenCalledTimes(1)
    expect(instance).toBeTruthy()
  })

  test("it can be call setup()", async () => {
    const instance = new mockAuthentication()
    await instance.setup()

    expect(mockSetup).toHaveBeenCalledTimes(1)
  })

  test.only("can set specific a provider by its key", () => {
    const instance = new mockAuthentication()
    instance.setupProviders()

    const provider = { name: "facebook", key: "facebook.com" }
    const res = instance.setProvider(provider.key)

    expect(mockSetupProviders).toHaveBeenCalledTimes(1)
    expect(res).toBeTruthy()
    // expect(res).toEqual({ kind: "ok", provider: provider })
  })
})

describe.skip("Provider", () => {
  test("can get list of predefiend simplified providers", () => {
    const simplifiedProviderList = Types.StaticsSimplified

    expect(simplifiedProviderList).toBeTruthy()
    expect(simplifiedProviderList).toHaveLength(3)
    expect(simplifiedProviderList).toMatchObject([
      { name: "facebook", key: "facebook.com" },
      { name: "google", key: "google.com" },
      { name: "twitter", key: "twitter.com" },
    ])
  })
})
