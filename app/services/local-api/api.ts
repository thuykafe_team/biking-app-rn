import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem, GeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"

const BASE_SEGMENT = "/users"
const convertUser = (raw: any): Types.User => {
  return {
    picture: raw.avatar || "https://via.placeholder.com/150",
    ...raw,
  }
}
/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: "application/json",
      },
    })
  }

  /**
   * Gets a list of users.
   */
  async getUsers(): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get("/users")

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data
      const resultUsers: Types.User[] = rawUsers.map(convertUser)
      return { kind: "ok", users: resultUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Gets a single user by token
   */

  async getInfo(token: string): Promise<Types.GetUserResult> {
    const url = BASE_SEGMENT + "/me"
    const response: ApiResponse<any> = await this.apisauce.get(
      url,
      {},
      { headers: { Authorization: "Bearer " + token } },
    )

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const resultUser: Types.User = convertUser(response.data)
      return { kind: "ok", user: resultUser }
    } catch {
      return { kind: "bad-data" }
    }
  }

  async fetchToken(auth: Types.BasicAuth): Promise<Types.GetTokenResult> {
    const url = BASE_SEGMENT + "/login"
    const response: ApiResponse<any> = await this.apisauce.post(url, auth)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      const token: string = response.data.token
      return { kind: "ok", token: token }
    } catch (error) {
      return { kind: "something-went-wrong", error }
    }
  }

  async signUp(userWithCre: Types.UserWithCredential): Promise<Types.GetUserResult> {
    const url = BASE_SEGMENT + "/signup"
    const response: ApiResponse<any> = await this.apisauce.post(url, userWithCre)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      const registeredUser: Types.User = response.data
      return { kind: "ok", user: registeredUser }
    } catch (error) {
      return { kind: "something-went-wrong", error }
    }
  }

  async resetPassword(email: string): Promise<boolean | GeneralApiProblem> {
    const url = BASE_SEGMENT + "/reset-password"
    const response: ApiResponse<any> = await this.apisauce.get(url, { email: email })

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    } else {
      return true
    }
    return false
  }
}
