import { GeneralApiProblem } from "./api-problem"

type gender = "male" | "female" | "other"

export interface User {
  id: number
  name?: string
  email: string
  firstName?: string
  lastName?: string
  avatar?: string
  age?: number
  gender?: gender
}

export interface UserWithCredential {
  password: string
  user: Omit<User, "id">
}

export type BasicAuth = {
  password: string
  email: string
}

export type GetUsersResult = { kind: "ok"; users: User[] } | GeneralApiProblem
export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem
export type GetTokenResult = { kind: "ok"; token: string } | GeneralApiProblem
